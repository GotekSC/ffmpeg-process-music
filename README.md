# ffmpeg-process-music
This is a little collection of shell scripts that will scan an input directory for .wav .m4a or .flac files, convert them to .flac 16bit 44100khz and .mp3 with qscale:3.

Then puts them into an output directory, while sorting the files according to this example tree:

```
~/.convertmusic
├── convertin
│   └── Ghost in the Shell (2017) Original Soundtrack - Lorne Balfe
│       ├── 1. Shelling.wav
│       ├── 2. Alternative Shelling Sequence.wav
│       ├── 3. Major on site.wav
│       ├── cover.jpg
│       ├── yGITS_altcover1.jpg
│       └── yGITS_altcover2.jpg
└── convertout
    └── Ghost in the Shell (2017) Original Soundtrack - Lorne Balfe
        ├── 1. Shelling.flac
        ├── 2. Alternative Shelling Sequence.flac
        ├── 3. Major on site.flac
        ├── art
        │   ├── yGITS_altcover1.jpg
        │   └── yGITS_altcover2.jpg
        ├── cover.jpg
        └── mp3
            ├── 1. Shelling.mp3
            ├── 2. Alternative Shelling Sequence.mp3
            └── 3. Major on site.mp3

6 directories, 15 files
```

The init script is currently hardcoded to launch 4 instances of the worker to utilize that number of cores. The worker script will also output logs into a specified directory.


## Installation
1. Clone/download this repository into a directory of your liking.
2. Change all declarations of the `indir` and `outdir` variables to your liking.
3. The paths to the other scripts in `ffmpeg-menu` and `anylossless2flac+mp3-init` are hardcoded, change them as necessary.

## Usage
* If you use rofi, launch the ffmpeg-menu script and you'll be presented with a custom rofi menu.
* Else just launch `anylossless2flac+mp3-init`.

## Credits
The ffmpeg-menu script is really just a modified version of rofi-bangs by gotbletu

<a href="http://www.youtube.com/watch?feature=player_embedded&v=kxJClZIXSnM
" target="_blank"><img src="http://img.youtube.com/vi/kxJClZIXSnM/0.jpg"
alt="Gotbletu rofi-bangs" width="240" height="180" border="10" /></a>

https://www.youtube.com/watch?v=kxJClZIXSnM
